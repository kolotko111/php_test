<?php

session_start();
require_once 'database.php';

if(isset($_SESSION['logged_id']))
{
	$name_stuff = filter_input(INPUT_POST, "name_stuff");
	$user_id = $_SESSION['logged_id'];
	$query = $db->prepare('INSERT INTO stuff values(null, :name_stuff, :user_id)');
	$query->bindValue(':name_stuff', $name_stuff, PDO::PARAM_STR);
	$query->bindValue(':user_id', $user_id, PDO::PARAM_STR);
	$query->execute();
	header('Location: stuff_list.php');
}
else
{
	header('Location: index.php');
}

?>