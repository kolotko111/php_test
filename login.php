<?php

session_start();
require_once 'database.php';


if(isset($_POST['email']))
{
	//sprawdzenie czy email jest prawidłowy
	$email = filter_input(INPUT_POST, 'email');
	$password = filter_input(INPUT_POST, 'pass1');
		
		
	$query = $db->prepare('SELECT id, user, pass, email FROM users WHERE email = :email');
	$query->bindValue(':email', $email, PDO::PARAM_STR);
	$query->execute();
		
	$returned_user = $query->fetch();
	if($returned_user && password_verify($password, $returned_user['pass']))
	{
		$_SESSION['logged_id'] = $returned_user['id'];
		unset($_SESSION['bad_attempt']);
		header('Location: stuff_list.php');
	}
	else
	{
		$_SESSION['given_email'] = $email;
		$_SESSION['bad_attempt'] = true;
		header('Location: index.php');
		exit();
	}

}
else
{
	header('Location: index.php');
	exit();
}

