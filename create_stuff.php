<?php

session_start();

if(!isset($_SESSION['logged_id']))
{
	header('Location: index.php');
}

?>

<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="utf-8">
    <title>Blog</title>
    <meta http-equiv="X-Ua-Compatible" content="IE=edge">
	
	<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

</head>

<body>	
	<div class="container">
		<div class="row justify-content-center">
							<div class="col-md-8">
								<div class="card">
									<div class="card-header">Add stuff</div>
									<div class="card-body">

										<form class="form-horizontal" method="post" action="create_stuff_save.php">

											<div class="form-group">
												<label for="name" class="cols-sm-2 control-label">Name</label>
												<div class="cols-sm-10">
													<div class="input-group">
														<span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
														<input type="text" class="form-control" name="name_stuff" id="name_stuff" placeholder="Name" />
													</div>
												</div>
											</div>
											
											<div class="form-group ">
												<button type="submit" class="btn btn-primary btn-lg btn-block login-button">Add</button>
											</div>
										</form>
									</div>

								</div>
							</div>
		</div>
	</div>
</body>
</html>