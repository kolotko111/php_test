<?php

session_start();
require_once 'database.php';

if(isset($_SESSION['logged_id']))
{
	if(isset($_GET['edit']))
	{
		$id = $_GET['edit'];
		$query = $db->query("select * from stuff where id = '$id'");
		$old_stuff = $query->fetch();
	}
	else
	{
		header('Location: stuff_list.php');
	}
}
else
{
	header('Location: index.php');
}
?>

<html lang="pl">
<head>
    <meta charset="utf-8">
    <title>Blog</title>
    <meta http-equiv="X-Ua-Compatible" content="IE=edge">

	<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>

<body>
	<div class="container">
		<div class="row justify-content-center">
							<div class="col-md-8">
								<div class="card">
									<div class="card-header">Add stuff</div>
									<div class="card-body">

										<form class="form-horizontal" method="post" action="edit_stuff_save.php">
										<input type="hidden" name="stuff_id" value="<?php echo $old_stuff['id'];?>"> 
										
											<div class="form-group">
												<label for="name" class="cols-sm-2 control-label">Old name: <?= $old_stuff['name']?></label>
											</div>

											<div class="form-group">
												<label for="name" class="cols-sm-2 control-label">New name</label>
												<div class="cols-sm-10">
													<div class="input-group">
														<span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
														<input type="text" class="form-control" name="new_stuff" id="new_stuff" placeholder="new name" />
													</div>
												</div>
											</div>
											
											<div class="form-group ">
												<button type="submit" class="btn btn-primary btn-lg btn-block login-button">Add</button>
											</div>
										</form>
									</div>

								</div>
							</div>
		</div>
	</div>
</body>
</html>