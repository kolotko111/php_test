<?php

session_start();
require_once 'database.php';

if(isset($_SESSION['logged_id']))
{
	if(isset($_GET['del']))
	{
		$stuff_id = $_GET['del'];
		$query = $db->prepare('DELETE FROM stuff WHERE id = :id');
		$query->bindValue(':id', $stuff_id, PDO::PARAM_INT);
		$query->execute();
		header('Location: stuff_list.php');
	}
	else
	{
		header('Location: stuff_list.php');
	}
}
else
{
	header('Location: index.php');
}