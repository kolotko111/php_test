<?php
session_start();

if(isset($_SESSION['logged_id']))
{
	header('Location: stuff_list.php');
}
?>
<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="utf-8">
    <title>Blog</title>
    <meta http-equiv="X-Ua-Compatible" content="IE=edge">
	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	
	 <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <link rel="stylesheet" href="index_style.css">
</head>

<body>
	<div class="wrapper fadeInDown">
		<div id="formContent">
			<header>
				<h1>Log in</h1>
			</header>

			<main>
				<article>
					<form method="post" action="login.php">
						<input type="email" id="login" class="fadeIn second" name="email" <?= isset($_SESSION['given_email']) ? 'value="'.$_SESSION['given_email'].'"' : ''?>>
						<input type="password" id="password" class="fadeIn third" name="pass1" placeholder="password">
						<input type="submit" class="fadeIn fourth" value="Log In">
						<?php
						if (isset($_SESSION['given_email']))
						{
							echo '<p>Incorrect email or password!</p>';
							unset($_SESSION['given_email']);
						}
						?>
						</br>
						<a href="registration.php">Registration</a>
					</form>
				</article>
			</main>
		</div>
	</div>
	<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
</body>
</html>