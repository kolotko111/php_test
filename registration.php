<?php
	session_start();

	//jeśli jestesmy 1 raz pomiń logikę
	if(isset($_POST['email']))
	{
		//wartość fałsz oznacza bład w formularzu
		$validation_ok = true;
		
		//weryfikacja nazwy użytkownika
		$nick = $_POST['nick'];
		$_SESSION['given_nick'] = $_POST['nick'];
		if((strlen($nick)<3) || (strlen($nick)>20))
		{
			$validation_ok=false;
			$_SESSION['e_nick']="Nick musi posiadać od 3 do 20 znaków";
		}
		
		if(ctype_alnum($nick) == false) 
		{
			$validation_ok=false;
			$_SESSION['e_nick']="Nick może się zkładać tylko z liter i cyfr";
		}
		
		//weryfikacja email
		$_SESSION['given_email'] = $_POST['email'];
		if(isset($_POST['email']))
		{
			$email = filter_input(INPUT_POST, "email", FILTER_VALIDATE_EMAIL);
			if(empty($email))
			{
				$validation_ok=false;
				$_SESSION['e_email'] = "Nieprawidłowy adres email";
			}
		}
		
		//walidacja hasła 
		$pass_first = $_POST['pass_first'];
		$pass_second = $_POST['pass_second'];
		$haslo_hash = password_hash($pass_first, PASSWORD_DEFAULT);
		
		if((strlen($pass_first)<8) || (strlen($pass_first)>20))
		{
			$validation_ok=false;
			$_SESSION['e_pass']="Haslo musi posiadać od 8 do 20 znaków";
		}
		
		if($pass_first != $pass_second)
		{
			$validation_ok=false;
			$_SESSION['e_pass']="Podane hasła muszą być takie same";
		}
		
		//sprawdzenie czy zaakceptowaliśmy regulamin
		// if(!isset($_POST['regulations']))
		// {
			// $validation_ok=false;
			// $_SESSION['e_regulations']="Regulamin musi być zaakceptowany";
		// }
		
		//sprawdzenie captcha
		// $secret = "6LcfCsgUAAAAAITsIaeCFzzJhCqa_f-wjLL2iGri";
		// $captcha_test = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
		
		// $odpowiedz = json_decode($captcha_test);
		
		// if($odpowiedz->success == false)
		// {
			// $validation_ok=false;
			// $_SESSION['e_bot']="Potwierdź że nie jesteś robotem";
		// }
		
		if($validation_ok == true)
		{
			//połączenie z db
			require_once 'database.php';
			//wstawienie do db
			$query = $db->prepare('INSERT INTO users values(null, :user, :pass, :email)');
			$query->bindValue(':user', $nick, PDO::PARAM_STR);
			$query->bindValue(':pass', $haslo_hash, PDO::PARAM_STR);
			$query->bindValue(':email', $email, PDO::PARAM_STR);
			$query->execute();
			unset($_SESSION['given_email']);
			unset($_SESSION['given_email']);
			header('Location: index.php');
			
		}
	}
	
?>

<!DOCTYPE HTML>
<html lang="pl">
<head>
	<meta charset="utf-8"?>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<title>Blog</title>
	<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	
	<script src="https://www.google.com/recaptcha/api.js" async defer></script>
	
	<style>
		.error
		{
			color:red;
			margin-top: 10px;
			margin-bottom: 10px;
		}
	</style>
</head>
<body>
	
	
	
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-8">
					<div class="card">
						<div class="card-header">Register</div>
							<div class="card-body">

                                <form class="form-horizontal" method="post">


                                    <div class="form-group">
                                        <label for="name" class="cols-sm-2 control-label">Nick</label>
                                        <div class="cols-sm-10">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                                                <input type="text" class="form-control" name="nick" id="nick" placeholder="Enter your nick" <?= isset($_SESSION['given_nick']) ? 'value="'.$_SESSION['given_nick'].'"' : ""?>/>
                                            </div>
											<?php
												if(isset($_SESSION['e_nick']))
												{
													echo '<div class = "error">'.$_SESSION['e_nick'].'</div>';
													unset($_SESSION['given_nick']);
													unset($_SESSION['e_nick']);
												}
											?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="email" class="cols-sm-2 control-label">Your Email</label>
                                        <div class="cols-sm-10">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
                                                <input type="text" class="form-control" name="email" id="email" placeholder="Enter your Email" <?= isset($_SESSION['given_email']) ? 'value="'.$_SESSION['given_email'].'"' : ""?>/>
                                            </div>
											<?php
												if(isset($_SESSION['e_email']))
												{
													echo '<div class = "error">'.$_SESSION['e_email'].'</div>';
													
													unset($_SESSION['e_email']);
												}
											?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="username" class="cols-sm-2 control-label">Password</label>
                                        <div class="cols-sm-10">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-users fa" aria-hidden="true"></i></span>
                                                <input type="password" class="form-control" name="pass_first" id="pass_first" placeholder="Enter your password" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="password" class="cols-sm-2 control-label">Confirm Password</label>
                                        <div class="cols-sm-10">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                                                <input type="password" class="form-control" name="pass_second" id="pass_second" placeholder="Enter your Password" />
                                            </div>
											<?php
												if(isset($_SESSION['e_pass']))
												{
													echo '<div class = "error">'.$_SESSION['e_pass'].'</div>';
													unset($_SESSION['e_pass']);
												}
											?>
                                        </div>
                                    </div>
									
									<!--<div class="g-recaptcha" data-sitekey="6LcfCsgUAAAAACAUXLeO1kUV5uyf8AG7U3KrJTXy"></div>-->
									<?php
										// if(isset($_SESSION['e_bot']))
										// {
											// echo '<div class = "error">'.$_SESSION['e_bot'].'</div>';
											// unset($_SESSION['e_bot']);
										// }
									?>
                                    </br>
                                    <div class="form-group ">
                                        <button type="submit" class="btn btn-primary btn-lg btn-block login-button">Register</button>
                                    </div>
									</br>
									<a href="index.php">Back</a>
								</form>
							</div>
					</div>
				</div>
			</div>
		</div>
</body>
</html>