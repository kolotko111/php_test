<?php
session_start();
require_once 'database.php';

if(isset($_SESSION['logged_id']))
{
	if(isset($_POST['new_stuff']))
	{
		$new_stuff = filter_input(INPUT_POST, 'new_stuff');
		$stuff_id =filter_input(INPUT_POST, 'stuff_id');
		

		$query = $db->prepare('UPDATE stuff SET name=:name WHERE id=:id');
		$query->bindValue(':name', $new_stuff, PDO::PARAM_STR);
		$query->bindValue(':id', $stuff_id, PDO::PARAM_INT);
		$query->execute();
		header('Location: stuff_list.php');
	}
	else
	{
		header('Location: stuff_list.php');
	}
}
else
{
	header('Location: index.php');
}
?>