<?php

$config = require_once 'config.php';

try
{
	$db = new PDO("mysql:host={$config['host']};dbname={$config['database']};charset=utf8", 
	$config['user'], 
	$config['password'], 
	//zapytanie i dane wysyłane oddzielnie, zwiększa odpornośc na wstrzykiwanie sql
	[PDO::ATTR_EMULATE_PREPARES => false, 
	//do debagowania
	PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);
}
catch(PDOException $error)
{
	exit('Database error');
}