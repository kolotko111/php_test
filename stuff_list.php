<?php

session_start();
require_once 'database.php';

if(isset($_SESSION['logged_id']))
{
	$user_id = $_SESSION['logged_id'];
	//nie wiem dlaczego nie mogę bind value dać 
	$query = $db->query("select * from stuff where user_id = '$user_id'");
	//$query->bindValue(':user_id', $user_id, PDO::PARAM_INT);
	$returned_stuff = $query->fetchAll();
}
else
{
	header('Location: index.php');
}
?>

<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="utf-8">
    <title>Blog</title>
    <meta http-equiv="X-Ua-Compatible" content="IE=edge">

	<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
	<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
	<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
	<link rel="stylesheet" href="stuff_list_style.css">
</head>

<body>
	<main>
		<a href="logout.php">Wyloguj</a>
		
		
		
		<div class="container">
			<div class="row col-md-6 col-md-offset-2 custyle">
			<table class="table table-striped custab">
				<thead>
				<a href="create_stuff.php" class="btn btn-primary btn-xs pull-right"><b>+</b> Add new stuff</a>
					<tr>
						<th>ID</th>
						<th>Name</th>
						<th class="text-center">Action</th>
					</tr>
				</thead>
					
					<?php
						foreach($returned_stuff as $item)
						{
							echo "<tr><td>{$item['id']}</td><td>{$item['name']}</td><td class='text-center'><a class='btn btn-info btn-xs' href='edit_stuff.php?edit={$item['id']}'><span class='glyphicon glyphicon-edit'></span>Edit</a> <a class='btn btn-danger btn-xs' href='delete_stuff.php?del={$item['id']}'><span class='glyphicon glyphicon-remove'></span>Del</a></td></tr>";
						}
					?>
			</table>
			</div>
		</div>
    </main>
</body>
</html>


